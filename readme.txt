Radyjonka - MediaWiki extension
===============================

The Radyjonka MediaWiki extension provides a basic support for creating
a progressive web app (PWA) from the MediaWiki instance. It also allows
to insert some content to the HTML head (linked data etc.).

Installation
------------

Download and place the files in a directory called Radyjonka in your
'extensions/' folder.

Add the following code at the bottom of your 'LocalSettings.php':

	wfLoadExtension( 'Radyjonka' );

Done. Navigate to 'Special:Version' on your wiki to verify that the extension
is successfully installed.

Configuration for linked data
-----------------------------

If you want to add some linked data to your site (probably because of improving
your site's search results), just add an associative array
to your 'LocalSettings.php':

	$wgRadyjonkaLinkedData = [
		"@context" => "https://schema.org",
		"@type" => "WebSite",
		"url" => "https://www.mymediawikiaddress.com/",
		"potentialAction" => [
			"@type" => "SearchAction",
			"target" => "https://www.mymediawikiaddress.com/index.php?search={search_term_string}",
			"query-input" => "required name=search_term_string"
		]
	];

This associative array will be translated into reasonable JSON
and inserted to the HTML head of your wiki.

For more inspiration how to use linked data for improving search
results please visit:

	https://developers.google.com/search/docs/data-types/article
	
Configuration for PWA
---------------------

Progressive web apps are websites that provide some offline content for
the user and can be installed on desktops or mobile phones and
feel like native apps of the device.

To enable progressive web app on your wiki, just insert this piece of code
to your 'LocalSettings.php':

	$wgRadyjonkaPWA = true;
	
Then you'll need to meet some criteria.

1.	You have to set your own web app's 'manifest.json' and place it in the root
	directory of your wiki.
	
	For documentation please visit this page:
	
	https://developers.google.com/web/fundamentals/web-app-manifest/
	
	For inspiration just visit our wiki's manifest:
	
	https://www.wikiskripta.eu/manifest.json
	
2.	You'll need two PNG icons, 192x192 px and 512x512 px (see 'manifest.json').
3.	You'll need some offline landing page that says something like:
	"You're offline now, please return to the home page."
4.	You'll need a javascript service worker named 'sw.js' in your root
	directory of the wiki (i.e. where the 'LocalSettings.php' file
	is stored). That service worker file declares, what is
	the behavior of the website when online and when offline.
	You can pre-cache some pages, you can cache pages on demand
	(like let user click on "Read later" button) or cache visited
	articles automatically.
	
	For documentation on service workers please visit:
	
	https://developers.google.com/web/fundamentals/primers/service-workers/
	
	For inspiration check the 'sw-example.js' file in this repository.
5.	Your site has to be served over a secure and valid HTTPS connection.
	Make sure that your HTTP version correctly redirects to HTTPS.

License
-------

Created by Petr Kajzar, 1st Faculty of Medicine, Charles Univesity,
Czech Republic, 2019. Released under Creative Commons Zero v1.0
Universal license.

To the extent possible under law, I have dedicated all copyright
and related and neighboring rights to this software to the public
domain worldwide. This software is distributed without any warranty.