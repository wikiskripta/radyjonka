<?php
/**
 * Hooks for Radyjonka extension
 *
 * @file
 * @ingroup Extensions
 * @author Petr Kajzar
 * @copyright 1st Faculty of Medicine, Charles University, Czech Republic
 * @license https://creativecommons.org/publicdomain/zero/1.0/ CC0-1.0
 */

class RadyjonkaHooks {

	/*
	 * Add linked data to page header.
	 *
	 * @param OutputPage $out Output page.
	 * @param string $text Wiki text, unused.
	 */
	public static function onOutputPageBeforeHTML(OutputPage &$out, &$text)
	{
		/* add linked data */
		$dataConfig = RequestContext::getMain()->getConfig()->get("RadyjonkaLinkedData");
		if ($dataConfig !== false) {
			$wikijsonld = '<script type="application/ld+json">' . PHP_EOL .
				json_encode($dataConfig, JSON_PRETTY_PRINT | JSON_FORCE_OBJECT | JSON_UNESCAPED_SLASHES) .
				PHP_EOL . '</script>';
			$out->addHeadItem("wikijsonld", $wikijsonld);
		}
		
		/* add manifest.json and sw.js */
		$pwaConfig = RequestContext::getMain()->getConfig()->get("RadyjonkaPWA");
		$scriptPath = RequestContext::getMain()->getConfig()->get("ScriptPath");
		if ($pwaConfig !== false) {
			/* manifest */
			$out->addLink([
				"rel" => "manifest",
				"href" => $scriptPath . "/manifest.json"
			]);

			/* service worker */
			$out->addInlineScript(
				'if ("serviceWorker" in navigator) {' . PHP_EOL .
				'navigator.serviceWorker.register("' . $scriptPath . '/sw.js");' . PHP_EOL .
				'}');
		}
	}
}